//
//  XYZAppDelegate.h
//  ToDoList
//
//  Created by 岡澤裕二 on 2014/04/18.
//
//

#import <UIKit/UIKit.h>

@interface XYZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
