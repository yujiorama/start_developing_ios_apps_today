//
//  main.m
//  ToDoList
//
//  Created by 岡澤裕二 on 2014/04/18.
//
//

#import <UIKit/UIKit.h>

#import "XYZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XYZAppDelegate class]));
    }
}
